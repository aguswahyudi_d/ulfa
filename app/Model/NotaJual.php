<?php 
class Notajual extends AppModel {	
	public $useTable = "notajuals";
	public $belongsTo = array('Pembeli'=>array(
						'className' => 'Pembelis',
						'foreignKey' => 'pembeli_id'
						),
					'User' =>array(
						'className'=>'Users',
						'foreignKey'=>'user_id'
					),
					'Toko' =>array(
						'className'=>'Tokos',
						'foreignKey'=>'toko_id'
					)
		);
	public $hasMany = array(
				'Transjual' => array(
						'className' => 'Transjuals',
						'foreignKey' => 'notajual_id',
            'dependent' => true
					)
			);
}

 ?>