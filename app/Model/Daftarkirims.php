<?php 
class Daftarkirims  extends AppModel {	
	public $useTable = "daftarkirims";
	public $belongsTo = array(
				'Gudang'=>array(
            'className' => 'Gudangs',
            'foreignKey' => 'gudang_id'
					),
				'Toko'=>array(
            'className' => 'Tokos',
            'foreignKey' => 'toko_id'
					)
			);
}

 ?>