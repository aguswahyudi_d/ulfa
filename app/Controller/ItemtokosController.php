<?php
class ItemtokosController extends AppController
{	
	public $layout = "basic";
	public $theme = "tema1";
	public $components = array('Paginator');
	public $uses = array('Toko','Itemtoko','Item','Hargaunit','Unit');
	public $helpers = array('Session');

	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('index','add','edit','delete','getdataitem');
	}
	
	public function isAuthorized($user) {
		if(isset($user['role']) && $user['role'] === 'manager toko') {
			return true;
		}
		return false;
	}
	
	public function index(){
		$this->set("title", 'Daftar Pembeli');
		$dataunits=$this->Unit->find('all');
		
		$this->Paginator->settings = array(
					'conditions'=>array('Itemtoko.toko_id ' => 1),
					'limit' => 5,
					'order' => array('Itemtoko.id' => 'desc')
				);
		$datas = $this->Paginator->paginate('Itemtoko');
		////debug($datas);
		$this->set(compact('datas','dataunits'));
	}

//JSON -------------------------------------------------------
	public function getdataitem($kode='',$nama='',$p='')
	{
		$this->autoRender = false;
		if($this->request->is('ajax'))
		{
			$conditions['AND']=array();
			if ($kode!='tidak_ada') {
				array_push($conditions['AND'],
		                		array('Item.kodebarang LIKE' => '%'.$kode.'%')
		                	);			                	
			}
			if ($nama!='tidak_ada') {
				array_push($conditions['AND'],
		                		array('Item.nama LIKE' => '%'.$nama.'%')
		                	);			                	
			}
			//if($kode!="tidak_ada"){
			$this->Paginator->settings = array(
					'limit' => 5,
					'order' => array('Item.id' => 'asc'),
					'conditions' => $conditions,
					'page'=>$p
				);
			$datas = $this->Paginator->paginate('Item');
				// $datas=$this->Item->find('all',array(
				//  	'conditions' => $this->conditions,//array('Item.kodebarang like "%'.$kode.'%" AND Item.nama like "%'.$nama.'%"'), //array of conditions
				//     'recursive' => 1
				// ));
			// }
			// else{
			// 	$datas['Item']['nama']="";
			// }
			
			if($datas)
			{
				echo json_encode($datas);
			}
			else
			{
				echo json_encode(array());
			}
		}
		else
		{
			$this->redirect(array('action'=>'index'));
		}
	}
// -----------------------------------------------------------


	public function add() {
		$this->set("title", 'Tambah Itemtoko');
		$datas=$this->Unit->find('all');
		$this->set(compact('datas'));
		////debug($datas);
		if ($this->request->is('post')) {
			// lakukan operasi insert
			$this->Itemtoko->create();
			$this->request->data['Itemtoko']['tanggal_masuk']=date("Y-m-d H:i:s");
			if ($this->Itemtoko->save($this->request->data)) {
				// jika insert berhasil
				$id=$this->Itemtoko->getLastInsertID();
				////debug($this->request->data);
				foreach ($this->request->data['Hargaunit'] as $data) {
					////debug($data);
					$this->Hargaunit->create();
					$this->request->data['Hargaunit']['itemtoko_id']=$id;
					$this->request->data['Hargaunit']['unit_id']=$data['unit_id'];
					if($data['harga']!=''){
						$this->request->data['Hargaunit']['harga']=$data['harga'];
					}
					else{
						$this->request->data['Hargaunit']['harga']='0';	
					}
					$this->Hargaunit->save($this->request->data);
				}

				$this->Session->setFlash('Tambah Itemtoko berhasil!', 
										 'default',
										 array('class'=>'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				// jika insert gagal
				// tetap tampilkan form
				// flash default adalah 'bad'
				$this->Session->setFlash('Ada kesalahan INSERT data Itemtoko.');
			}
		}
	}

	public function delete($id = null) {
		if ($this->request->is('post')) {
			if ($id) {
				$data = $this->Itemtoko->findById($id);
				$this->Itemtoko->id = $id;
				if ($this->Itemtoko->delete()) {
					$this->Session->setFlash('Data sudah terhapus', 'default',
											array('class'=>'success'));
				}
			}
		}
		$this->redirect(array('action'=>'index'));
	}

	public function edit($id = null){
		$this->set('title', 'Edit Item Toko');
		if ($this->request->is('post')) {
			// lakukan operasi UPDATE				
			if ($this->request->data) {				
				$this->Itemtoko->id = $this->request->data['Itemtoko']['id'];
				if ($this->Itemtoko->save($this->request->data)) {					
					foreach ($this->request->data['Hargaunit'] as $data) {
						$this->Hargaunit->id=$data['id'];
						if($data['harga']==''){
							$data['harga']='0';
						}						
						$this->Hargaunit->save($data);
					}
					$this->Session->setFlash('Sunting data telah tersimpan.',
											 'default',
											 array('class'=>'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Maaf, sunting data gagal.');
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
			}
			
		} else {
			if ($id) {
				try {
					$data = $this->Itemtoko->read(null, $id);
					//$this->set(compact('data'));
					$this->request->data = $data;
					$datas=$this->Unit->find('all');
					$this->set(compact('datas'));		
				} catch (NotFoundException $ex) {
					$this->Session->setFlash('Data tidak ditemukan.');
					$this->redirect(array('action'=>'index'));
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
				$this->redirect(array('action'=>'index'));
			}
		}
	}
	
}
?>

