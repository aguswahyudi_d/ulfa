<?php
class DaftarkirimsController extends AppController
{	
	public $layout = "basic";
	public $theme = "tema1";
	public $components = array('Paginator');
	public $uses = array('Daftarkirim','Gudang','Toko','Item');
	public $helpers = array('Session');


    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index','add','edit','delete','detil');
    }
    
	public function index(){
		$this->set("title", 'Daftar Kirim');
		
		$this->Paginator->settings = array(
					'limit' => 5,
					'order' => array('Daftarkirim.id' => 'asc'),
					'group'=>array('Daftarkirim.tanggal_kirim','Daftarkirim.toko_id','Daftarkirim.status','Daftarkirim.tanggal_terima')
				);
		$datas = $this->Paginator->paginate('Daftarkirim');
		////debug($datas);

		$daftartoko=$this->Toko->find('list',
			array('fields' => array('Toko.id','Toko.nama'),
				'recursive'=>0));

		$this->set(compact('datas','daftartoko'));
	}

	
	public function add() {
		$this->set("title", 'Tambah Daftar Kirim');
		if ($this->request->is('post')) {
			// lakukan operasi insert
			//debug($this->request->data);
			$count=0;
			foreach ($this->request->data['Daftarkirim'] as $value) {		
			//debug($value)			;
				$this->Daftarkirim->create();
				$this->request->data['Daftarkirim']['tanggal_kirim']=date("Y-m-d H:i:s");

				$this->request->data['Daftarkirim']['gudang_id']=$value['gudang_id'];
				$this->request->data['Daftarkirim']['jumlah']=$value['jumlah'];
				$this->request->data['Daftarkirim']['toko_id']=$this->request->data['Toko']['id'];
				$this->request->data['Daftarkirim']['tanggal_terima']='';
				$this->request->data['Daftarkirim']['status']='perjalanan';
				$this->Daftarkirim->save($this->request->data);
			}
			$this->Session->setFlash('Tambah Daftar Kirim berhasil!', 
										 'default',
										 array('class'=>'success'));
			$this->redirect(array('controller'=>'gudangs','action' => 'kirimbarang'));

			//$this->Pembeli->create();
			// if ($this->Pembeli->save($this->request->data)) {
			// 	// jika insert berhasil

			// 	$this->Session->setFlash('Tambah Pembeli berhasil!', 
			// 							 'default',
			// 							 array('class'=>'success'));
			// 	//$this->redirect(array('action' => 'index'));
			// } else {
			// 	// jika insert gagal
			// 	// tetap tampilkan form
			// 	// flash default adalah 'bad'
			// 	$this->Session->setFlash('Ada kesalahan INSERT data Pembeli.');
			// }
		}
	}

	public function delete($id = null) {
		if ($this->request->is('post')) {
			if ($id) {
				$data = $this->Daftarkirim->findById($id);
				
				$this->Daftarkirim->id = $id;
				if ($this->Daftarkirim->delete()) {
					$this->Session->setFlash('Data sudah terhapus', 'default',
											array('class'=>'success'));
				}
			}
		}
		 $this->redirect($this->referer());
		//$this->redirect(array('action'=>'index'));
	}

	public function edit($id = null){
		$this->set('title', 'Edit Daftarkirim');
		if ($this->request->is('post')) {
			//debug($this->request->data);

			$count=0;
			foreach ($this->request->data['Daftarkirim'] as $value) {		
			////debug($value);
				$this->Daftarkirim->create();
				$this->request->data['Daftarkirim']['tanggal_kirim']=$value['tanggal_kirim'];
				$this->request->data['Daftarkirim']['id']=$value['id'];
				$this->request->data['Daftarkirim']['gudang_id']=$value['gudang_id'];
				$this->request->data['Daftarkirim']['jumlah']=$value['jumlah'];
				$this->request->data['Daftarkirim']['toko_id']=$this->request->data['Toko']['toko_id'];
				
				if($value['status']=="perjalanan")
				$this->request->data['Daftarkirim']['tanggal_terima']="";
				else
				$this->request->data['Daftarkirim']['tanggal_terima']=$value['tanggal_terima'];
				$this->request->data['Daftarkirim']['status']=$value['status'];
				
				$this->Daftarkirim->save($this->request->data);
			}
			$this->Session->setFlash('Tambah Daftar Kirim berhasil!', 
										 'default',
										 array('class'=>'success'));
			$this->redirect(array('controller'=>'gudangs','action' => 'kirimbarang'));
		} else {
			if ($id) {
				try {
					$data = $this->Pembeli->read(null, $id);
					//$this->set(compact('data'));
					$this->request->data = $data;
				} catch (NotFoundException $ex) {
					$this->Session->setFlash('Data tidak ditemukan.');
					$this->redirect(array('action'=>'index'));
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
				$this->redirect(array('action'=>'index'));
			}
		}
	}

	public function detil($id='')
	{
		$date=$this->Daftarkirim->find('first',array('conditions'=>array('Daftarkirim.id'=>$id)));
		$this->Paginator->settings = array(
					'order' => array('Daftarkirim.id' => 'asc'),
					'conditions'=>array('Daftarkirim.tanggal_kirim'=>$date['Daftarkirim']['tanggal_kirim'],'Daftarkirim.toko_id'=>$date['Daftarkirim']['toko_id'])
				);
		$datas = $this->Paginator->paginate('Daftarkirim');
		

		$daftartoko=$this->Toko->find('list',
			array('fields' => array('Toko.id','Toko.nama'),
				'recursive'=>0));
		$no=0;
		foreach($datas as $data) {
			$temp=$this->Item->find('first',
				array('conditions'=>array('Gudangs.id'=>$data['Daftarkirim']['gudang_id']),
					'recursive'=>1
					)
				);
			////debug($temp);
			$datas[$no]['Daftarkirim']['kodebarang']=$temp['Item']['kodebarang'];
			$datas[$no]['Daftarkirim']['nama']=$temp['Item']['nama'];			
			$no=$no+1;
		}
		$this->set(compact('datas','daftartoko','date'));
	}

}
?>

