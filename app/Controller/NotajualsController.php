<?php
class NotajualsController extends AppController
{	
	public $layout = "basic";
	public $theme = "tema1";
	public $components = array('Paginator');
	public $uses = array('Pembeli','Notajual','Notajual','Transjual','Item','Itemtoko','User','Unit','Hargaunit');
	public $helpers = array('Session');


	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('index','detil','edit','delete','edit_transjual','edit_notajual','getdataitem');
	}
	
	public function isAuthorized($user) {
		if(isset($user['role']) && in_array($user['role'], array('owner', 'manager toko'))) {
			return true;
		}
		return false;
	}
    
	public function index(){
        $this->conditions = array(); //Transform POST into GET 
		if(($this->request->is('post') || $this->request->is('put')) && isset($this->data['Filter'])){
	        $filter_url['controller'] = $this->request->params['controller'];
			$filter_url['action'] = $this->request->params['action'];
	        // We need to overwrite the page every time we change the parameters
	        $filter_url['page'] = 1;

			// for each filter we will add a GET parameter for the generated url
	        foreach($this->data['Filter'] as $name => $value){
	        	if($value){
	            	// You might want to sanitize the $value here
	                // or even do a urlencode to be sure
	                $filter_url[$name] = urlencode($value);
	            }
			}       
	        // now that we have generated an url with GET parameters, 
	        // we'll redirect to that page
	        return $this->redirect($filter_url);                
		}else{
		// Inspect all the named parameters to apply the filters
			$this->conditions['OR']=array();
			$this->conditions['AND']=array();
        	foreach($this->params['named'] as $param_name => $value){
	            // Don't apply the default named parameters used for pagination
	            ////debug($this->params['named']);
	            if(!in_array($param_name, array('page','sort','direction','limit'))){
	            	// You may use a switch here to make special filters
	            	// like "between dates", "greater than", etc
	                if($param_name == "search"){
	                	array_push($this->conditions['OR'],
	                	array('Pembeli.nama LIKE' => '%' . $value . '%'),	                    
	                    array('User.username LIKE' => '%' . $value . '%')
	                	);
					}elseif ($param_name == "status") {
						if ($value=='1') {
							array_push($this->conditions['AND'],
		                		array('Notajual.status LIKE' => '%lunas%')
		                	);			                	
						}elseif ($value=='2') {
							array_push($this->conditions['AND'],
		                		array('Notajual.status LIKE' => '%hutang%')
		                	);	
						}
					} 
					else {
	            		$this->conditions['Notajual.'.$param_name] = $value;
	            	}                                       
	            	$this->request->data['Filter'][$param_name] = $value;
	        	}
    		}
		}
			////debug($this->Session->read('kondisi'));
		$this->set("title", 'Daftar Transaksi Jual');
		
		$this->Paginator->settings = array(
			 		'limit' => 5,
					'order' => array('Notajual.id' => 'desc'),
					'conditions' => $this->conditions
				);
		
		$datas = $this->Paginator->paginate('Notajual');
		////debug($datas);
		$this->set(compact('datas'));
		$this->set('search', isset($this->params['named']['search']) ? $this->params['named']['search'] : "");
	}

//JSON -------------------------------------------------------
	public function getdataitem($kode='',$nama='',$p='')
	{
		$this->autoRender = false;
		if($this->request->is('ajax'))
		{
			$conditions['AND']=array();
			if ($kode!='tidak_ada') {
				array_push($conditions['AND'],
		                		array('Item.kodebarang LIKE' => '%'.$kode.'%')
		                	);			                	
			}
			if ($nama!='tidak_ada') {
				array_push($conditions['AND'],
		                		array('Item.nama LIKE' => '%'.$nama.'%')
		                	);			                	
			}
			//if($kode!="tidak_ada"){
			$this->Paginator->settings = array(
					'limit' => 5,
					'order' => array('Itemtoko.id' => 'asc'),
					'conditions' => $conditions,
					'page'=>$p
				);
			$datas = $this->Paginator->paginate('Itemtoko');
				// $datas=$this->Item->find('all',array(
				//  	'conditions' => $this->conditions,//array('Item.kodebarang like "%'.$kode.'%" AND Item.nama like "%'.$nama.'%"'), //array of conditions
				//     'recursive' => 1
				// ));
			// }
			// else{
			// 	$datas['Item']['nama']="";
			// }
			
			if($datas)
			{
				echo json_encode($datas);
			}
			else
			{
				echo json_encode(array());
			}
		}
		else
		{
			$this->redirect(array('action'=>'index'));
		}
	}
// -----------------------------------------------------------

	public function detil($id=''){
		$this->set("title", 'Detil Transaksi Jual');
		
		$this->Paginator->settings = array(
					'limit' => 5,
					'order' => array('Transjual.id' => 'asc'),
					'conditions' => array('Transjual.notajual_id' => $id),
					'recursive'=>2
				);
		$datas = $this->Paginator->paginate('Transjual');
		$temp=$this->Notajual->find('first',array(
			'conditions' => array('Notajual.id' => $id),
			'recursive'=>1));
		$nama_user=$this->User->findById($temp['Notajual']['user_id']);
		$nama_pembeli=$this->Pembeli->findById($temp['Notajual']['pembeli_id']);
		$jenis_unit = $this->Unit->find('list',
			array('fields' => array('Unit.id','Unit.nama')
							));	
		$isi_unit=	$this->Unit->find('list',
			array('fields' => array('Unit.id','Unit.isi')
							));	
		$hargaunit=$this->Hargaunit->find('list',
			array('fields' => array('Hargaunit.unit_id','Hargaunit.harga','Hargaunit.itemtoko_id'))
			);
		////debug($datas);
		$this->set(compact('datas','jenis_unit','hargaunit','isi_unit','temp','nama_user','nama_pembeli'));
		
	}

	public function delete($id = null) {
		if ($this->request->is('post')) {
			if ($id) {
				$data = $this->Notajual->findById($id);				
				$this->Notajual->id = $id;
				if ($this->Notajual->delete()) {
					$this->Session->setFlash('Data sudah terhapus', 'default',
											array('class'=>'success'));
				}
			}
		}
		$this->redirect(array('action'=>'index'));
	}

	public function edit_transjual($id = null){
		$this->set('title', 'Edit Transjual');
		if ($this->request->is('post')) {
			// lakukan operasi UPDATE		
			//debug($this->request->data);
			if ($this->request->data) {
				$this->Transjual->id = $this->request->data['Transjual']['id'];
				if ($this->Transjual->save($this->request->data)) {
					$this->Session->setFlash('Sunting data telah tersimpan.',
											 'default',
											 array('class'=>'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Maaf, sunting data gagal.');
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
			}
			//$this->redirect(array('action'=>'index'));
		} else {
			if ($id) {
				try {
					$data = $this->Transjual->read(null, $id);
					$item=$this->Item->find('first',array('conditions'=>array('Item.id'=>$data['Itemtoko']['item_id'])));
					//$this->set(compact('data'));
					$this->request->data = $data;
					$this->request->data['Unit']=$this->Unit->find('list',array(
						'fields' => array('Unit.id','Unit.nama'),
						'recursive'=>0));
					$this->request->data['jenis_unit'] = $this->Unit->find('list',
					array('fields' => array('Unit.nama')
									));
					$this->request->data['Item']=$item['Item'];					
					$this->request->data['Hargaunit']=$this->Hargaunit->find('list',array(
						'fields' => array('Hargaunit.unit_id','Hargaunit.harga'),
						'conditions'=>array('Hargaunit.itemtoko_id'=>$data['Itemtoko']['id'])));
					
				} catch (NotFoundException $ex) {
					$this->Session->setFlash('Data tidak ditemukan.');
					$this->redirect(array('action'=>'index'));
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
				$this->redirect(array('action'=>'index'));
			}
		}
	}

	public function edit($id=null){
		$this->set('title', 'Edit Notajual');
		if ($this->request->is('post')) {
			// lakukan operasi UPDATE				
			if ($this->request->data) {
				$this->Notajual->id = $this->request->data['Notajual']['id'];
				if($this->request->data['Notajual']['hutang']<=0){
					$this->request->data['Notajual']['status']='lunas';
				}
				if ($this->Notajual->save($this->request->data)) {

					$this->Session->setFlash('Sunting data telah tersimpan.',
											 'default',
											 array('class'=>'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Maaf, sunting data gagal.');
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
			}
			$this->redirect(array('action'=>'index'));
		} else {
			if ($id) {
				try {
					$data = $this->Notajual->read(null, $id);
					//$this->set(compact('data'));
					$this->request->data = $data;
				} catch (NotFoundException $ex) {
					$this->Session->setFlash('Data tidak ditemukan.');
					$this->redirect(array('action'=>'index'));
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
				$this->redirect(array('action'=>'index'));
			}
		}

	}
	
	public function cetak($id='') {
		
		$this->layout = "cetak_nota";
		$this->theme = NULL;
		
		$this->set("title", 'Detil Transaksi Jual');
		
		$this->Paginator->settings = array(
					
					'order' => array('Transjual.id' => 'asc'),
					'conditions' => array('Transjual.notajual_id' => $id),
					'recursive'=>2
				);
		$datas = $this->Paginator->paginate('Transjual');
		$temp=$this->Notajual->find('first',array(
			'conditions' => array('Notajual.id' => $id),
			'recursive'=>1));
		$nama_user=$this->User->findById($temp['Notajual']['user_id']);
		$nama_pembeli=$this->Pembeli->findById($temp['Notajual']['pembeli_id']);
		$jenis_unit = $this->Unit->find('list',
			array('fields' => array('Unit.id','Unit.nama')
							));	
		$isi_unit=	$this->Unit->find('list',
			array('fields' => array('Unit.id','Unit.isi')
							));	
		$hargaunit=$this->Hargaunit->find('list',
			array('fields' => array('Hargaunit.unit_id','Hargaunit.harga','Hargaunit.itemtoko_id'))
			);
		////debug($datas);
		$this->set(compact('datas','jenis_unit','hargaunit','isi_unit','temp','nama_user','nama_pembeli'));
	}

}
?>

