<?php
class TransjualsController extends AppController
{	
	public $layout = "basic";
	public $theme = "tema1";
	public $components = array('Paginator');
	public $uses = array('Notajual','Transjual','Item','Itemtoko','Unit','Hargaunit');
	public $helpers = array('Session');


	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('index','daftartransaksi','getdatainput','getdata','add','addAll');
	}
	
	public function isAuthorized($user) {
		if(isset($user['role']) && in_array($user['role'], array('owner', 'manager toko', 'kasir'))) {
			return true;
		}
		return false;
	}

	public function index(){
		$this->set("title", 'Transaksi');	
		$jenis_unit = $this->Unit->find('list',
			array('fields' => array('Unit.nama')
							));
		
		$this->set(compact('jenis_unit'));
	}

//JSON -------------------------------------------------------
	// public function getdata($kode,$no)
	// {
	// 	$this->autoRender = false;
	// 	if($this->request->is('ajax'))
	// 	{
	// 		$datas=$this->Itemtoko->find('first',array(
	// 			 	'conditions' => array('Item.kodebarang' => $kode), //array of conditions
	// 			    'recursive' => 1
	// 			));
	// 		$datas['no']=$no;
	// 		if($datas)
	// 		{
	// 			echo json_encode($datas);
	// 		}
	// 		else
	// 		{
	// 			echo json_encode(array());
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$this->redirect(array('action'=>'index'));
	// 	}
	// }
// -----------------------------------------------------------

//HTML -------------------------------------------------------
	public function add()
	{
		$this->layout='ajax';
		//$hasil = $_POST['keyword'];
		//$keyword='%'.$hasil.'%';		
			// $this->paginate = array(
			// 'conditions' => array(
			//     'OR' => array(
			//         'Gudangs.kodebarang LIKE' => '%'. $keyword . '%'
			//         )
			//     ),'limit' => 10
			// );
			// $list = $this->paginate('Gudangs');
			// $this->set('list',$list);
			//$this->set('hasil',$_POST['keyword']);
			$this->set('kode',$_POST['kode']);
			$this->set('nama',$_POST['nama']);
			$this->set('jumlah',$_POST['jumlah']);
			$this->set('unit',$_POST['unit']);
			$this->set('harga',$_POST['harga']);
			$this->set('total',$_POST['total']);
			$this->set('itemtoko_id',$_POST['itemtoko_id']);
			$this->set('no',$_POST['no']);
			$jenis_unit = $this->Unit->find('list',array('fields' => array('Unit.nama')));		
			$this->set(compact('jenis_unit'));
			
	}
// -----------------------------------------------------------

public function addAll()
{
		if ($this->request->is('post')) {
			// lakukan operasi insert
			$this->Notajual->create();						
			$this->request->data['Notajual']['user_id'] = 3;
			$this->request->data['Notajual']['pembeli_id'] = 1;
			$this->request->data['Notajual']['tanggal'] = date("Y-m-d H:i:s");
			$this->request->data['Notajual']['harga_total'] =$this->request->data['totalharga'] ;
			$this->request->data['Notajual']['keuntungan_total'] = $this->request->data['untungharga'];
			$this->request->data['Notajual']['dibayar'] = $this->request->data['bayarharga'];
			$this->request->data['Notajual']['hutang']=$this->request->data['hutangharga'];
			$this->request->data['Notajual']['jatuh_tempo']=$this->request->data['jatuhtempo'];
			if($this->request->data['hutangharga'])
			$this->request->data['Notajual']['status']="hutang";			
			else
				$this->request->data['Notajual']['status']="lunas";
			$this->request->data['Notajual']['toko_id']=$this->Session->read('Auth.User.toko_id');
			debug($this->request->data);
			if ($this->Notajual->save($this->request->data)) {
				
				$count=0;
				foreach ($this->request->data['Transjual'] as $value) {		
				debug($value)			;
					$this->Transjual->create();
					$this->request->data['Transjual']['notajual_id']=$this->Notajual->getLastInsertID();
					$this->request->data['Transjual']['itemtoko_id']=$value['itemtoko_id'];
					$this->request->data['Transjual']['quantity']=$value['quantity'];
					$this->request->data['Transjual']['unit']=$value['unit'];
					$this->request->data['Transjual']['total_harga_jual']=$value['total'];
					$this->request->data['Transjual']['keuntungan']=$value['keuntungan'];
					$this->Transjual->save($this->request->data);
				}
				// jika insert berhasil
				$this->redirect(array('action' => 'index'));

			} else {
				$this->Session->setFlash('Ada kesalahan INSERT data Pembeli.');
			}
		}
}

//JSON -------------------------------------------------------
	public function getdatainput($kode)
	{
		$this->autoRender = false;
		if($this->request->is('ajax'))
		{
			if($kode!="tidak_ada"){
				$datas=$this->Itemtoko->find('first',array(
				 	'conditions' => array('Item.kodebarang like "%'.$kode.'%"'), //array of conditions
				    'recursive' => 2
				));
				$datas['isi_unit']=$this->Unit->find('list',
					array('fields' => array('Unit.isi')
					));
				$datas['kode_barang']=$this->Item->find('all',array(
						'conditions' => array('Item.kodebarang like "%'.$kode.'%"'), //array of conditions
						'recursive'=>0
					));
			}
			else{
				$datas['Item']['nama']="";
			}
			if($datas)
			{
				echo json_encode($datas);
			}
			else
			{
				echo json_encode(array());
			}
		}
		else
		{
			$this->redirect(array('action'=>'index'));
		}
	}
// -----------------------------------------------------------

	public function delete($id = null) {
		if ($this->request->is('post')) {
			if ($id) {
				$data = $this->Pembeli->findById($id);
				
				$this->Pembeli->id = $id;
				if ($this->Pembeli->delete()) {
					$this->Session->setFlash('Data sudah terhapus', 'default',
											array('class'=>'success'));
				}
			}
		}
		$this->redirect(array('action'=>'index'));
	}

	public function edit($id = null){
		$this->set('title', 'Edit Pembeli');
		if ($this->request->is('post')) {
			// lakukan operasi UPDATE				
			if ($this->request->data) {
				$this->Pembeli->id = $this->request->data['Pembeli']['id'];
				if ($this->Pembeli->save($this->request->data)) {
					$this->Session->setFlash('Sunting data telah tersimpan.',
											 'default',
											 array('class'=>'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Maaf, sunting data gagal.');
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
			}
			$this->redirect(array('action'=>'index'));
		} else {
			if ($id) {
				try {
					$data = $this->Pembeli->read(null, $id);
					//$this->set(compact('data'));
					$this->request->data = $data;
				} catch (NotFoundException $ex) {
					$this->Session->setFlash('Data tidak ditemukan.');
					$this->redirect(array('action'=>'index'));
				}
			} else {
				$this->Session->setFlash('Permintaan tidak valid.');
				$this->redirect(array('action'=>'index'));
			}
		}
	}

}
?>

