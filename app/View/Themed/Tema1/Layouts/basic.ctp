<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title><?php
	if (isset($title)){
		echo $title;
	} 
	?></title>

	<?php echo $this->Html->script('jquery');?>
	<?php echo $this->Html->script('index');?>
	<?php echo $this->Html->script('jquery-ui-1.10.4.custom.min'); ?>
	<?php echo $this->Html->script('jquery.hotkeys'); ?>
	
	<?php echo $this->Html->script('bootstrap.min'); ?>
	<?php echo $this->Html->css(array('cake.generic.css','bootstrap.min.css','jquery-ui-1.10.4.custom.min.css','me.css')); ?>
</head>
<body>
	

	<div id="wrap" style="padding-top:5%;">
		
	<?php echo $this->element('navbar', array('menu'=> strtolower($this->params['controller'])) ); ?> 
		
		<div class="container">
			<?php 
		// 		if (strpos($this->request->url, 'main') !== true && 
	 //  		    $this->request->here !== $this->request->base . '/') {
		// echo $this->Html->getCrumbList(array(
		// 				'firstClass' => 'firstHome',
		// 				'class' => 'breadcrumb'
		// 			));
		//  }
		if (strpos($this->request->url, 'main') === false && strpos($this->request->url, 'transjuals') === false &&
	  		$this->request->here !== $this->request->base . '/') 
				echo $this->Html->getCrumbs(' > ', 'Home'); 
	/*echo $this->Html->getCrumbs(' > ', array(
    'text' => $this->Html->image('home.png'),
    'url' => array('controller' => 'pages', 'action' => 'display', 'home'),
    'escape' => false
	));*/

	?>
		<?php

		echo $this->Session->flash();
		echo $this->fetch('content');
		?>
		</div>
		
		<?php
		echo $this->element('footer');
		?>
	</div>
	
	<?php
	echo $this->Html->script(array( 'jquery.autocomplete.min' ));
	?>
	
	<script type="text/javascript">
	$(document).ready(function(){
	<?php echo $this->element('jsmain'); ?>
	<?php echo $this->element('jstransjual'); ?>
	<?php echo $this->element('jsnotajual'); ?>
	<?php echo $this->element('jsitemtoko'); ?>
	<?php echo $this->element('jsgudang'); ?>
	<?php echo $this->element('jsdaftarkirim'); ?>
	});
	</script>
	<?php echo $scripts_for_layout; ?>
	<!-- Js writeBuffer -->
	<?php
	if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
	// Writes cached scripts
	?>
</body>
</html>