<?php
if (strtolower($this->params['controller']) === 'transjuals') {
?>
//variabel
    var kodebarang="";
    var jumlah;
    var unit;
    var harga_total=0;
    var keuntungan=0;
    var hutang=0;
    var bayar=0;
    var autocomplete_data;
    var no=0;
////
    $('.submit').hide();
    
    $('.cetak-nota-jual').on('click',function(){        
        $('#TransjualAddForm').submit();
    });
    
    $('#TransjualAddForm').on('submit',function(){
       $('.input_select').removeAttr("disabled");
       $(".formatAngka").each(function(){
                    $(this).val(replaceChars($(this).val()));
        });
    });

    $('.bayar-nota-jual').on('click',function(){
        $(".bayar-harga").focus();
    });
        
    $.event.special.inputchange = {
        setup: function() {
            var self = this, val;
            $.data(this, 'timer', window.setInterval(function() {
                val = self.value;
                if ( $.data( self, 'cache') != val ) {
                    $.data( self, 'cache', val );
                    $( self ).trigger( 'inputchange' );                    
                }
                $(".formatAngka").each(function(){
                    $(this).val(formatAngka($(this).val()));
                });
            }, 20));
        },
        teardown: function() {
            window.clearInterval( $.data(this, 'timer') );
        },
        add: function() {
            $.data(this, 'cache', this.value);
        }
    };

    var tampdata;
    $(".jatuh-tempo").hide();
    $(".hutang-harga").on('inputchange', function() {
        if($(".hutang-harga").val()>0){
            $(".jatuh-tempo").show();
        }
        else $(".jatuh-tempo").hide();
    });    

    $(".input-kode-barang").on('inputchange', function() {
        //$( "form:first" ).trigger( "submit" );
        
        var kodebarang=$(".input-kode-barang").val();
        if(!kodebarang){
            kodebarang="tidak_ada";
        }
        $.ajax({
            dataType: 'json',
            url: '<?php echo $this->Html->url(array('action'=>'getdatainput')); ?>/'+kodebarang
        }).done(function(datas){
            tampdata=datas;
            $(".input-nama-barang").val(datas['Item']['nama']);
            if(datas['Item']['nama'])
            {               
                $(".input-jumlah-barang").val("1");
                datas['Hargaunit'].forEach(function(data){
                    if(data['unit_id']==$(".input-unit-barang").val()){
                        $(".input-harga-barang").val(
                        data['harga']
                        );
                        $(".input-total-barang").val(data['harga']*1);                    
                    }
                });
                
                $(".input-unit-barang").children().first().attr("selected","selected");

            }            
            else
            {            
                $(".input-jumlah-barang").val("");   
                $(".input-harga-barang").val("");
                $(".input-total-barang").val("");
            }
        });         
    });

    $(".input-jumlah-barang").on('inputchange',function(){
        if($(".input-kode-barang").val()){
            replaceChars($(".input-total-barang").val(
                replaceChars($(".input-harga-barang").val())*replaceChars($(".input-jumlah-barang").val())
            ));
        }
    });

    $(".input-unit-barang").on('inputchange',function(){
        tampdata['Hargaunit'].forEach(function(data){
            if(data['unit_id']==$(".input-unit-barang").val())
                replaceChars($(".input-harga-barang").val(
                data['harga']
                ));
        });
        

        $(".input-total-barang").val(
            replaceChars($(".input-harga-barang").val())*replaceChars($(".input-jumlah-barang").val())
        );
    });

    $(".tambah-nota-jual").on('click', function() {
        if($(".input-kode-barang").val()!=''){
            kodebarang=$(".input-kode-barang").val();
            unit=$(".input-unit-barang").val();
            no++;
            $(".input-kode-barang").focus();
            console.log(tampdata);
            $.ajax({
                type: "POST",
                data:{itemtoko_id:tampdata['Itemtoko']['id'],kode:$(".input-kode-barang").val(),nama:$(".input-nama-barang").val(),jumlah:$(".input-jumlah-barang").val(),unit:$(".input-unit-barang").val(),harga:$(".input-harga-barang").val(),total:$(".input-total-barang").val(),no:no},
                url: "<?php echo $this->base;?>/transjuals/add/",
                success:function(data) {           
                    $(".last-row-nota-jual").after(data);
                    console.log(tampdata);
                    $('#Transjual'+no+'Keuntungan').val(
                        parseInt(
                            replaceChars(
                                $(".input-total-barang").val()))
                        -
                        parseInt(
                            replaceChars(
                                $(".input-jumlah-barang").val()
                            )
                        )
                        *
                        parseInt(
                            tampdata['Itemtoko']['hargabeli']
                        )
                        *
                        parseInt(
                            tampdata['isi_unit'][$(".input-unit-barang").val()]
                        )
                    );
                    console.log(tampdata['isi_unit'][$(".input-unit-barang").val()]);
                    console.log(tampdata['Itemtoko']['hargabeli']);
                    keuntungan+=parseInt($('#Transjual'+no+'Keuntungan').val());
                    $('.untung-harga').val(keuntungan);

                    $("#Transjual"+no+"Unit option[value='"+$(".input-unit-barang").val()+"']").attr('selected','selected');
                    $('.view-total-harga-transaksi').html($(".input-nama-barang").val()+"<br>Rp. "+$(".input-total-barang").val()+",00");

                    $(".input-jumlah-barang").val("");
                    $(".input-kode-barang").val("");
                    $(".input-unit-barang").val(0);
                    harga_total+=parseInt(replaceChars($("#Transjual"+no+"Total").val()));
                    $('.total-harga').val(harga_total);
                    console.log(harga_total);
                }
            }); 
        }            
    });
 
    var sisa=0;
    $(".bayar-harga").on('inputchange',function(){
        if($(".bayar-harga").val()!=''){
            sisa=parseInt(parseInt(replaceChars($(".bayar-harga").val()))-parseInt(replaceChars($(".total-harga").val())));
            if(sisa<0){
                $(".kembali-harga").val('0');
                $(".hutang-harga").val(parseInt(replaceChars($(".total-harga").val()))-parseInt(replaceChars($(".bayar-harga").val())));
            }
            else{
                $(".hutang-harga").val(0);
                $(".kembali-harga").val(sisa);
            }
        }else{
                $(".hutang-harga").val(0);
                $(".kembali-harga").val(0);
        }
        
    });

//edit    
    /*$( document ).on( "click", ".hapus", function() {        
        var id=$(this).parent().parent().attr("class");
        $('#Transjual'+id+'Kode').removeAttr("disabled");
        $('#Transjual'+id+'Quantity').removeAttr("disabled");
        $('#Transjual'+id+'Unit').removeAttr("disabled");
        console.log();
    });*/
//

//hapus
    $( document ).on( "click", ".hapus", function() {        
        var tamp=$(this).parent().parent().attr("class");
        harga_total-=parseInt($("#Transjual"+tamp+"Total").val());
        $('.total-harga').val(harga_total);
        keuntungan-=parseInt($('#Transjual'+tamp+'Keuntungan').val());
        $('.untung-harga').val(keuntungan);
        $(this).parent().parent().remove();
    });
//

//formatAngka
    function replaceChars(entry) {
        out = "."; // replace this
        add = ""; // with this
        temp = "" + entry; // temporary holder

        while (temp.indexOf(out)>-1) {
        pos= temp.indexOf(out);
        temp = "" + (temp.substring(0, pos) + add + 
        temp.substring((pos + out.length), temp.length));
        }
        return temp;
    }

    function formatAngka(angkaInput) {
        var angka=replaceChars(angkaInput);
        if (typeof(angka) != 'string') angka = angka.toString();
            var reg = new RegExp('([0-9]+)([0-9]{3})');
        while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
        return angka;
    }
//   
    
        
    
  

<?php } ?>
