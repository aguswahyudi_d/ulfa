<div class="row">
	<div class="col-md-2">
		<!-- <div class="btn-group-vertical" role="group" aria-label="...">
			 <?php echo $this->Form->Button('lihat laporan stok', array('onclick' => "location.href='Gudangs/lihatsemua'",'type'=>'button','class'=>'btn btn-default'));?>
			  <?php echo $this->Form->Button('lihat laporan stok', array('onclick' => "location.href='Gudangs/lihatsemua'",'type'=>'button','class'=>'btn btn-default'));?>
			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      Pembelian
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			      <li><?php echo $this->Html->link( "Lihat hutang gudang",   array('controller' => 'Notabeli', 'action' => 'showhutang')); ?></li>
			      <li><?php echo $this->Html->link( "Lihat informasi Supplier",   array('controller' => 'Penyedia', 'action' => 'index')); ?></li>
			      <li><?php echo $this->Html->link( "Sistem Pembelian",   array('controller' => 'Notabeli', 'action' => 'index') ); ?></li>
			      <li><?php echo $this->Html->link( "Tambah informasi Supplier",   array('controller' => 'Penyedia', 'action' => 'tambah') ); ?></li>
			    </ul>
			  </div>
			  
			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      Stok Gudang
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			      <li><?php echo $this->Html->link( "lihat harga barang",   array('controller' => 'Transbeli', 'action' => 'lihatharga') ); ?></li>
			      <li><?php echo $this->Html->link( "lihat stok barang terbaru",   array('controller' => 'Gudangs', 'action' => 'terbaru') ); ?></li>
			      <li><?php echo $this->Html->link( "lihat semua stok barang",   array('controller' => 'Gudangs', 'action' => 'lihatsemua') ); ?></li>
			      <li><?php echo $this->Html->link( "lihat barang tanpa harga",   array('controller' => 'Gudangs', 'action' => 'tanpaharga') ); ?></li>
			      <li><?php echo $this->Html->link( "tambah barang di gudang",   array('controller' => 'Gudangs', 'action' => 'update') ); ?></li>
			    </ul>
			  </div>

			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      laporan gudang
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			     <li><?php echo $this->Html->link( "Laporan update stok",   array('controller' => 'Laporanbarangs', 'action' => 'lihat') ); ?></li>
			     <li><?php echo $this->Html->link( "Laporan stok bulanan",   array('controller' => 'Transbeli', 'action' => 'stokbulanan') ); ?></li>
			     <li><?php echo $this->Html->link( "Laporan total pembelian stok bulanan",   array('controller' => 'Transbeli', 'action' => 'stokbulanan') ); ?></li>
			     <li><a href="#">Dropdown link</a></li>
			    </ul>
			  </div>


			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      Kirim barang
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			      <li><?php echo $this->Html->link( "kirim barang",   array('controller' => 'Gudangs', 'action' => 'kirimbarang') ); ?></li>
			      <li><?php echo $this->Html->link( "lihat daftar pengiriman barang",   array('controller' => 'daftarkirims', 'action' => 'index') ); ?></li>
			    </ul>
			  </div>
			</div> -->
			<?php echo $this->element('vertikalmenu'); ?>
	</div>
	<div class="col-md-5">
		<div class="row">
			<div class="col-md-12"><h3>Kirim Barang</h3></div>
			<?php echo $this->Form->create('Daftarkirim', array('action'=>'add', 'enctype' => 'multipart/form-data','class'=>'daftarkirimform')); ?>
			<div class="col-md-12">
				<?php echo $this->Form->input('Toko.id', array(
															'label' => 'Toko Tujuan Pengiriman : ',
															'type' => 'select',
															'options' => $daftar_toko,
															'class'=>'input_select input-toko',
															'default'=>'Pilih Toko',
															'empty'=>'Pilih Toko',
															'value'=>'0'
														));
							?>
			</div>
			
			<div class="col-md-12 isi-daftar-barang">
				<table>
					<thead>
						<tr>
							<td>Menu</td>
							<td>Jumlah</td>
							<td>Kode Barang</td>
							<td>Nama Barang</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<?php echo $this->Form->input('Gudang.id', array('label' => '','class'=>'input-gudang_id','value'=>'','readonly'=>'readonly','type'=>'hidden')); ?>
							<td><?php echo $this->Form->input('Gudang.jumlah', array('label' => '','class'=>'input-jumlah','value'=>'','readonly'=>'')); ?></td>
							<td><?php echo $this->Form->input('Gudang.kodebarang', array('label' => '','class'=>'input-kodebarang','value'=>'','readonly'=>'readonly')); ?></td>
							<td><?php echo $this->Form->input('Gudang.nama', array('label' => '','class'=>'input-nama','value'=>'','readonly'=>'readonly')); ?></td>
						</tr>
						<tr><td></td><td></td><td><a class="tambah-item-kirim">Tambah</a></td><td><a class="kirim-item-kirim">Kirim</a></td></tr>
						<tr class="last-row-kirim">

						</tr>
					</tbody>
				</table>
				<?php echo $this->Form->end('Simpan'); ?>	
			</div>				
		</div>
	</div>
	<div class="col-md-5">
		<div class="row">

			<div class="col-md-6"><?php echo $this->Form->input('search_kodebarang', array('label' => array('class' => 'sr-only','text' => 'Enter KeyWord'),'placeholder'=>'Kode Barang','class'=>'me-change search_kodebarang')); ?></div><div class="col-md-6"><?php echo $this->Form->input('search_nama', array('label' => array('class' => 'sr-only','text' => 'Enter KeyWord'),'placeholder'=>'Nama Barang','class'=>'me-change search_nama')); ?></div>
			<div class="col-md-12 ajax-view"></div>
			<div class="col-md-12">
				<p>
					<a class="prev" style="cursor:pointer;"> << prev </a>|		
					<a class="next" style="cursor:pointer;">next >></a>
				</p>

			</div>
		</div>
	</div>
</div> 