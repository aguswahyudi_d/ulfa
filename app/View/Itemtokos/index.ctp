<?php  $this->Html->addCrumb('Toko', array('controller' => 'Tokos', 'action' => 'index')); ?>
<?php  $this->Html->addCrumb('Daftar Item Toko','#'); ?>
<div class="row">
	<div class="col-md-12">
	<header>
		<h3>Daftar Item di Toko</h3>
		</header>
		<?php 
		echo $this->Html->link(
					'Tambah Item Toko', 
					array('controller'=>'Itemtokos', 'action' => 'add'),
					array('class' => 'btn btn-primary')
				);
		?>
		<table>
			<thead>
				<tr>
					<td></td>
					<td>Kode Barang</td>
					<td>Nama</td>
					<td>Jumlah</td>
					<td>Harga Beli</td>
					<?php foreach($dataunits as $data) { ?>
					<td><?= $data['Unit']['nama']; ?></td>
					<?php } ?>
					<td>Tanggal Masuk</td>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($datas as $data) {
			?>
				<tr>
					<td><?php 
					 echo $this->Html->link('Sunting', array('action'=>'edit', $data['Itemtoko']['id']));
					 echo $this->Form->postLink(' | Hapus', 
									array('action'=>'delete', $data['Itemtoko']['id']),
									array('confirm' => 'Apakah yakin akan menghapus data ' . 
										  $data['Item']['kodebarang']));
					// echo $this->Html->link(' | Detil', 
					// 				array('action'=>'detil', $data['Pembeli']['id']));
					 ?></td>
					<!-- <td>
						 <?php //echo 
						// $this->Html->link($data['Pembeli']['id'], '#', 
						// 			array('class'=>'anim btn btn-warning', 'nim' => $data['Pembeli']['id'])); 
						?>
					</td> -->
					
					<td><?= $data['Item']['kodebarang']; ?></td>
					<td><?= $data['Item']['nama']; ?></td>
					<td><?= $data['Itemtoko']['quantity']; ?></td>
					<td><?= $data['Itemtoko']['hargabeli']; ?></td>
					<?php foreach($data['Hargaunit'] as $dat) { ?>
					<td><?= $dat['harga']; ?></td>
					<?php } ?>
					<td><?= $data['Itemtoko']['tanggal_masuk']; ?></td>

				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
		<div class="paging">
			<?php
			echo $this->Paginator->prev(). ' ' . 
			     $this->Paginator->numbers(array('before'=>false, 'after'=>false,'separator'=>false)) . ' ' .
				 $this->Paginator->next();
			?>
		</div>

<!-- 		<div id="infodetil" style="margin: 40px auto; padding: 60px 20px; background-color: #31B0D5;">
		[Detil Siswa]
		</div>
 -->	</div>
</div>