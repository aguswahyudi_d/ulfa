<div class="row">
	<div class="col-md-12">		
		<header>
			<h3>Nota Jual</h3>
		</header>
	</div>
	
	<div class="col-md-12 tabel-nota-jual">
		<form action="/ulfa/transjuals/addAll" enctype="multipart/form-data" id="TransjualAddForm" method="post" accept-charset="utf-8">
			<div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
			
		<table>
			<thead>
				<tr>
					<td></td>
					<td>Kode</td>
					<td>Nama</td>
					<td>Jumlah</td>
					<td>Unit</td>
					<td>Harga</td>
					<td>Total</td>					
				</tr>
			</thead>
			<tbody><tr class="input-row-nota-jual">

					<td></td>									
					<td><?php echo $this->Form->input('kodebarang', array('label' => '','class'=>'input-kode-barang biginput','id'=>"autocomplete")); ?></td>			
					<td><?php echo $this->Form->input('nama', array('label' => '','class'=>'input-nama-barang ','disabled'=>'disabled')); ?></td>	
					<td><?php echo $this->Form->input('jumlah', array('label' => '','class'=>'input-jumlah-barang formatAngka')); ?></td>
					<td><?php echo $this->Form->input('unit', array(
													'label' => '',
													'type' => 'select',
													'options' => $jenis_unit,
													'class'=>'input-unit-barang',
													'default'=>'1'
												));
					?></td>
					<!-- <td><?php //echo $this->Form->input('unit',array('label'=>'','class'=>'input-unit-barang','options'=>array("satuan","pack-3","stgh-lusin","lusin","grosir"))); ?></td> -->
					<td><?php echo $this->Form->input('harga', array('label' => '','class'=>'input-harga-barang formatAngka','disabled'=>'disabled')); ?></td>	
					<td><?php echo $this->Form->input('total', array('label' => '','class'=>'input-total-barang formatAngka','disabled'=>'disabled')); ?></td>		

				</tr>
				<tr class="last-row-nota-jual">
					<td>
						<a href="#" class="tambah-nota-jual">Tambah</a>
					</td>					
					<td><a href="#" class="bayar-nota-jual">Bayar</a></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>				
			</tbody>
			
		</table>
			
	</div>
	<div class="col-md-9 pull-left">
		<div class="row">
			
			<div class="col-md-12 view-total-harga-transaksi"></div>
		</div>
	</div>
	<div class="col-md-1">Total</div>
	<div class="col-md-2 proses-transaksi" >
		<?php echo $this->Form->input('totalharga', array('label' => '','class'=>'total-harga formatAngka','readonly'=>'readonly')); ?></td>		
	</div>

	<div class="col-md-1">Bayar</div>
	<div class="col-md-2 proses-transaksi" >
		<?php echo $this->Form->input('bayarharga', array('label' => '','class'=>'bayar-harga formatAngka')); ?></td>		
	</div>
	<div class="col-md-3"><hr></div>
	<div class="col-md-1">Kembali</div>
	<div class="col-md-2 proses-transaksi" >
		<?php echo $this->Form->input('kembaliharga', array('label' => '','class'=>'kembali-harga formatAngka','readonly'=>'readonly')); ?></td>		
	</div>
	
	<div class="col-md-1">Hutang</div>
	<div class="col-md-2 proses-transaksi" >
		<?php echo $this->Form->input('hutangharga', array('label' => '','class'=>'hutang-harga formatAngka','readonly'=>'readonly')); ?></td>		
	</div>	
		<?php echo $this->Form->input('untungharga', array('label' => '','class'=>'untung-harga','readonly'=>'readonly','type'=>'hidden')); ?></td>		
	<!-- <a href="#" class="cetak-nota-jual">Cetak</a> -->
	
	<br>
		<div class="col-md-6 col-md-offset-6 jatuh-tempo" >
			<?php echo $this->Form->input('jatuhtempo', array('label' => 'Jatuh Tempo : ','type'=>'datetime','class'=>'jatuh_tempo-harga')); ?></td>		
	</div>
	<div class="submit"><input type="submit" value="Simpan"></div>			
			</form>			
</div>
