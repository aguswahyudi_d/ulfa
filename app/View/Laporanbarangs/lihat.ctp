
<div class="row">
	<div class="col-md-14">
		<div class="row">
		 <div class="col-md-2">
		  	<!-- <div class="btn-group-vertical" role="group" aria-label="...">
			 <?php echo $this->Form->Button('lihat laporan stok', array('onclick' => "location.href='Gudangs/lihatsemua'",'type'=>'button','class'=>'btn btn-default'));?>
			  <?php echo $this->Form->Button('lihat laporan stok', array('onclick' => "location.href='Gudangs/lihatsemua'",'type'=>'button','class'=>'btn btn-default'));?>
			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      Pembelian
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			      <li><?php echo $this->Html->link( "Lihat hutang gudang",   array('controller' => 'Notabeli', 'action' => 'showhutang')); ?></li>
			      <li><?php echo $this->Html->link( "Lihat informasi Supplier",   array('controller' => 'Penyedia', 'action' => 'index')); ?></li>
			      <li><?php echo $this->Html->link( "Sistem Pembelian",   array('controller' => 'Notabeli', 'action' => 'index') ); ?></li>
			      <li><?php echo $this->Html->link( "Tambah informasi Supplier",   array('controller' => 'Penyedia', 'action' => 'tambah') ); ?></li>
			    </ul>
			  </div>
			  
			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      Stok Gudang
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			      <li><?php echo $this->Html->link( "lihat stok barang terbaru",   array('controller' => 'Gudangs', 'action' => 'terbaru') ); ?></li>
			      <li><?php echo $this->Html->link( "lihat semua stok barang",   array('controller' => 'Gudangs', 'action' => 'lihatsemua') ); ?></li>
			      <li><?php echo $this->Html->link( "lihat barang tanpa harga",   array('controller' => 'Gudangs', 'action' => 'tanpaharga') ); ?></li>
			      <li><?php echo $this->Html->link( "tambah barang di gudang",   array('controller' => 'Gudangs', 'action' => 'update') ); ?></li>
			    </ul>
			  </div>

			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			      laporan gudang
			      <span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">
			     <li><?php echo $this->Html->link( "Laporan update stok",   array('controller' => 'Laporanbarangs', 'action' => 'lihat') ); ?></li>
			     <li><?php echo $this->Html->link( "Laporan stok bulanan",   array('controller' => 'Transbeli', 'action' => 'stokbulanan') ); ?></li>
			     <li><?php echo $this->Html->link( "Laporan total pembelian stok bulanan",   array('controller' => 'Transbeli', 'action' => 'stokbulanan') ); ?></li>
			     <li><a href="#">Dropdown link</a></li>
			    </ul>
			  </div>
			</div> -->
			<?php echo $this->element('vertikalmenu'); ?>
		  </div>
		  <div class="col-md-10">
		  	<div class='col-md-12 search-input'>
		  		<h2>Pencarian</h2>
		  		<?php
		  			 echo $this->Form->create("Search",array("default"=>false, 'id'=>'SearchForm','placeholder'=>'masukan kode barang'));
					 echo $this->Form->input('kode barang',array('type'=>'text','div'=>false,'id'=>'keyword','class'=>'form-control'));
					 
					 echo $this->Form->input('tanggal mulai', array(
			           'id'=>'datepicker',
			           'type'=>'text',
			           'class'=>'mulai form-control'
        			));
					 echo $this->Form->input('tanggal selesai', array(
			           'id'=>'datepicker2',
			           'type'=>'text',
			           'class'=>'akhir form-control'
        			));
					 echo $this->Form->submit('Search');
					 echo $this->Form->end();	

		  		?>
		  		
		  	</div>
			<script>
			$(function() {
			       $("#datepicker").datepicker({
			       	dateFormat: 'yy-mm-dd'
			       });
			       $("#datepicker2").datepicker({
			       	dateFormat: 'yy-mm-dd'
			       });
			});
			</script>
		  	<script type="text/javascript">
		  	$(document).ready(function(){
		  	 	$(document).on('submit','#SearchForm',function(){


				 $.ajax({
				   type: "POST",
				   data:{keyword:$("#keyword").val(),mulai:$('.mulai').val(),akhir:$('.akhir').val()},
				   url: "<?php echo $this->base;?>/Laporanbarangs/ajax_see/",
				   success:function(data) {
				   	
				   		$("#result").html(data);
				      }
				   });        
				 	      
				});

				
			});
		  	</script>
			<div id='result'>
		  	<table class='table table-bordered'>
		  		<thead>
		  			<tr>
		  				
		  				<th style='width:10%;'>kode barang</th>
		  				<th style='width:5%;'>stok</th>
		  				<th style='width:12%;'>satuan grosir</th>
		  				<th style='width:10%;'>lusin grosir</th>
		  				<th style='width:10%;'>lebih 6 lusin</th>
		  				<th style='width:12%;'>satuan eceran</th>
		  				<th style='width:10%;'>3pcs eceran</th>
		  				<th style='width:10%;'>lebih 1 lusin</th>
		  				<th style='width:15%;'>tgl perubahan</th>
		  				<th style='width:15%;'>keterangan</th>
		  			</tr>
		  		</thead>
		  		<tbody>
		  			<?php

		  					foreach ($data_gudang as $data) 
		  					{
		  			?>
		  						<tr>
									
								 <td><?php	echo	$data['Laporanbarangs']['kodebarang'];?></td>	
								 <td><?php	echo	$data['Laporanbarangs']['quantity'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['satuan_grosir'];?></td>		
								 <td><?php	echo	$data['Laporanbarangs']['lusin_grosir'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['lusin6_grosir'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['satuan_eceran'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['pcs3_eceran'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['lusin1_eceran'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['tanggal_aksi'];?></td>
								 <td><?php	echo	$data['Laporanbarangs']['keterangan'];?></td>
								 <td>			
								</tr>
		  			<?php 	
		  					}
		  			?>
				</tbody>


		  	</table>
		  	</div>
		  </div>
		  
		</div>

	</div>
</div>