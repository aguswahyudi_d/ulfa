<div class="row">
	<div class="col-md-12">
	<header>
		<h3>Detil Daftar Kirim</h3>
		</header>
		<p>
		<?php
		echo $this->Html->link('Kembali', array('action'=>'index'));
		?>
		</p>
		<?php echo $this->Form->create('Daftarkirim', array('action'=>'edit', 'enctype' => 'multipart/form-data','class'=>'editdaftarkirimform')); ?>
		<h3><?= $daftartoko[$date['Daftarkirim']['toko_id']]; ?></h3>
		<?php echo $this->Form->input('Toko.toko_id', array('type'=>'hidden','label' => '','value'=>$date['Daftarkirim']['toko_id'])); ?>
		<table>
			<thead>
				<tr>
					<td></td>
					<td>Jumlah</td>
					<td>Tanggal Kirim</td>
					<td>Kode Barang</td>
					<td>Nama</td>
					<td>Tanggal Terima</td>
					<td>Status</td>					
				</tr>
			</thead>
			<tbody>
				<?php
				//debug($datas);

				foreach($datas as $data) {
				?>

					<tr class=<?php echo $data['Daftarkirim']['id']; ?>>

						<td><a class="edit">Edit</a><?php echo $this->Form->postLink(' Hapus', 
									array('class'=>'hapus','action'=>'delete', $data['Daftarkirim']['id']),
									array('confirm' => 'Apakah yakin akan menghapus data ' . 
										 $data['Daftarkirim']['nama'])); ?>
						</td>					
						<?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.id', array('type'=>'hidden','label' => '','class'=>'input_select'.$data['Daftarkirim']['id'],'disabled'=>'disabled','value'=>$data['Daftarkirim']['id'])); ?>
						<?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.gudang_id', array('type'=>'hidden','label' => '','class'=>'input_select'.$data['Daftarkirim']['id'],'disabled'=>'disabled','value'=>$data['Daftarkirim']['gudang_id'])); ?>
						<td><?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.jumlah', array('label' => '','class'=>'input_select'.$data['Daftarkirim']['id'],'disabled'=>'disabled','value'=>$data['Daftarkirim']['jumlah'])); ?></td>						
						<td><?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.tanggal_kirim', array('type'=>'datetime','label' => '','class'=>'me-disabled','selected'=>$data['Daftarkirim']['tanggal_kirim'],'disabled'=>'disabled')); ?></td>
						<td><?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.kodebarang', array('label' => '','class'=>'input_select'.$data['Daftarkirim']['id'],'value'=>$data['Daftarkirim']['kodebarang'],'disabled'=>'disabled')); ?></td>
						<td><?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.nama', array('label' => '','class'=>'input_select'.$data['Daftarkirim']['id'],'disabled'=>'disabled','value'=>$data['Daftarkirim']['nama'])); ?></td>
						<td><?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.tanggal_terima', array('label' => '','class'=>'me-disabled input_select'.$data['Daftarkirim']['id'],'disabled'=>'disabled','selected'=>$data['Daftarkirim']['tanggal_terima'],'type'=>'datetime')); ?></td>
						<td><?php echo $this->Form->input('Daftarkirim.'.$data['Daftarkirim']['id'].'.status', array('label' => '','class'=>'input_select'.$data['Daftarkirim']['id'],'disabled'=>'disabled','value'=>$data['Daftarkirim']['status'])); ?></td>
						
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>		
		<a class="save-edit-daftar">Simpan</a>
		<?php echo $this->Form->end('Simpan'); ?>	
	</div>
</div>