<div class="row">
	<div class="col-md-6">
		<h2>Edit Trans Jual</h2>
		<p>
		<?php
		echo $this->Html->link('Kembali', array('action'=>'index'));
		?>
		</p>
		<p>
			<a class="lihat-daftar-item" style="cursor:pointer;">Lihat Daftar Item</a>
		</p>
<!-- 

					<td>Kode Barang</td>
					<td>Nama Barang</td>
					<td>Jumlah<br>(tiap unit)</td>
					<td>Unit</td>	
					<td>Isi Item<br>(tiap unit)</td>				
					<td>Harga Jual<br>(tiap unit)</td>
					<td>Total<br>Harga</td>
					<td>Harga Beli<br>(tiap unit)</td>
					<td>Total<br>Keuntungan</td> -->
		<?php
		debug($this->request->data);
		echo $this->Form->create('Notajual', array('action'=>'edit_transjual', 'enctype' => 'multipart/form-data'));

		echo $this->Form->input('Transjual.id', array('type' => 'hidden'));
		echo $this->Form->input('Transjual.itemtoko_id', array('type' => 'hidden','class'=>'input-tambah-item_id','readonly'=>'readonly','value'=>$this->request->data['Itemtoko']['id']));
		echo $this->Form->input('Item.kodebarang', array('label' => 'Kode Barang : ','class'=>'input-tambah-kodebarang','readonly'=>'readonly'));
		echo $this->Form->input('Item.nama', array('label' => 'Nama Barang : ','class'=>'input-tambah-nama','readonly'=>'readonly'));		
		echo $this->Form->input('Transjual.quantity', array('label' => 'Jumlah (tiap unit) : '));
		echo $this->Form->input('Transjual.unit', array(
													'label' => 'Unit : ',
													'type' => 'select',
													'options' => $this->request->data['jenis_unit'],
													'class'=>'input-unit-barang',
													'default'=>'1',
													'value'=>$this->request->data['Transjual']['unit']
												));
		echo $this->Form->input('Transjual.total_harga_jual', array('label' => 'Total Harga Jual : ','readonly'=>'readonly'));
		echo $this->Form->input('Transjual.keuntungan', array('label' => 'Keuntungan : ','readonly'=>'readonly'));
							
		echo $this->Form->end('Simpan');
		?>
	</div>
	<div class="col-md-3"><?php echo $this->Form->input('search_kodebarang', array('label' => array('class' => 'sr-only','text' => 'Enter KeyWord'),'placeholder'=>'Kode Barang','class'=>'me-change search_kodebarang')); ?></div><div class="col-md-3"><?php echo $this->Form->input('search_nama', array('label' => array('class' => 'sr-only','text' => 'Enter KeyWord'),'placeholder'=>'Nama Barang','class'=>'me-change search_nama')); ?></div>
	<div class="col-md-6 ajax-view"></div>
	<div class="col-md-6">
		<p>
			<a class="prev" style="cursor:pointer;"> << prev</a>			
			<a class="next" style="cursor:pointer;">next>></a>
		</p>

	</div>
</div>