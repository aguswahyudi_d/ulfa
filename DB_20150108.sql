-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2015 at 03:51 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ulfa_shops`
--
CREATE DATABASE IF NOT EXISTS `ulfa_shops` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ulfa_shops`;

-- --------------------------------------------------------

--
-- Table structure for table `gudangs`
--

CREATE TABLE IF NOT EXISTS `gudangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `satuan_grosir` int(11) NOT NULL,
  `lusin_grosir` int(11) NOT NULL,
  `lusin6_grosir` int(11) NOT NULL,
  `satuan_eceran` int(11) NOT NULL,
  `pcs3_eceran` int(11) NOT NULL,
  `lusin1_eceran` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `item_id` int(11) NOT NULL,
  `kodebarang` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `gudangs`
--

INSERT INTO `gudangs` (`id`, `quantity`, `satuan_grosir`, `lusin_grosir`, `lusin6_grosir`, `satuan_eceran`, `pcs3_eceran`, `lusin1_eceran`, `tanggal_masuk`, `item_id`, `kodebarang`) VALUES
(10, 1, 1, 1, 1, 1, 1, 100, '2014-12-19', 9, 'kl01rbs'),
(11, 100, 1, 1, 1, 1, 1, 1, '2014-12-06', 11, 'kl01rck'),
(12, 0, 1, 1, 1, 1, 1, 100, '2014-12-19', 13, 'kl02rbb');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodebarang` varchar(15) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nama_gambar` varchar(55) NOT NULL,
  `mime_type` varchar(55) NOT NULL,
  `file_path` varchar(150) NOT NULL,
  `transbeli_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `kodebarang`, `kategori_id`, `supplier_id`, `nama`, `nama_gambar`, `mime_type`, `file_path`, `transbeli_id`, `item_id`) VALUES
(9, 'kl01rbs', 63, 0, 'rumbai besi', 'gambar.jpg', 'image/jpeg', 'C:\\xampp\\htdocs\\cake-php\\app\\\\files\\photos\\Items\\', 0, 0),
(11, 'kl01rck', 63, 0, 'rumbai coklat', 'gambar.jpg', 'image/jpeg', 'C:\\xampp\\htdocs\\cake-php\\app\\\\files\\photos\\Items\\', 0, 0),
(12, 'kl01rbk', 63, 0, 'rumbai kuningan', 'gambar.jpg', 'image/jpeg', 'C:\\xampp\\htdocs\\cake-php\\app\\\\files\\photos\\Items\\', 0, 0),
(13, 'kl02rbb', 65, 0, 'rumbai biru', 'gambar.jpg', 'image/jpeg', 'C:\\xampp\\htdocs\\cake-php\\app\\\\files\\photos\\Items\\', 0, 0),
(14, 'kl02rbm', 65, 0, 'rumbai merah', 'gambar.jpg', 'image/jpeg', 'C:\\xampp\\htdocs\\cake-php\\app\\\\files\\photos\\Items\\', 0, 0),
(15, 'kl02rbu', 65, 0, 'rumbai ungu', 'gambar.jpg', 'image/jpeg', 'C:\\xampp\\htdocs\\cake-php\\app\\\\files\\photos\\Items\\', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategoris`
--

CREATE TABLE IF NOT EXISTS `kategoris` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `kategoris`
--

INSERT INTO `kategoris` (`id`, `nama`, `parent`) VALUES
(62, 'kalung', 0),
(63, 'etnik', 62),
(64, 'gelang', 0),
(65, 'besi', 64);

-- --------------------------------------------------------

--
-- Table structure for table `laporanbarangs`
--

CREATE TABLE IF NOT EXISTS `laporanbarangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodebarang` varchar(55) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `tanggal_aksi` datetime NOT NULL,
  `satuan_grosir` int(11) NOT NULL,
  `lusin_grosir` int(11) NOT NULL,
  `lusin6_grosir` int(11) NOT NULL,
  `satuan_eceran` int(11) NOT NULL,
  `pcs3_eceran` int(11) NOT NULL,
  `lusin1_eceran` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `gudangs_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `laporanbarangs`
--

INSERT INTO `laporanbarangs` (`id`, `kodebarang`, `keterangan`, `tanggal_aksi`, `satuan_grosir`, `lusin_grosir`, `lusin6_grosir`, `satuan_eceran`, `pcs3_eceran`, `lusin1_eceran`, `quantity`, `gudangs_id`) VALUES
(14, 'kl02rbb', 'ubah harga eceran', '0000-00-00 00:00:00', 1, 1, 1, 1, 1, 10, 0, 0),
(15, 'kl02rbb', 'ubah harga eceran', '0000-00-00 00:00:00', 1, 1, 1, 1, 1, 100, 0, 0),
(16, 'kl01rbs', 'ubah harga eceran', '0000-00-00 00:00:00', 1, 1, 1, 1, 1, 100, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notabelis`
--

CREATE TABLE IF NOT EXISTS `notabelis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penyedia_id` int(33) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `notabelis`
--

INSERT INTO `notabelis` (`id`, `penyedia_id`, `total_bayar`, `tanggal`) VALUES
(1, 1, 3000, '2014-11-04 17:00:00'),
(17, 3, NULL, '0000-00-00 00:00:00'),
(20, 24, NULL, '2014-11-16 06:32:55'),
(21, 3, NULL, '2014-11-19 02:36:00'),
(22, 0, NULL, '2014-11-20 07:08:08'),
(23, 0, NULL, '2014-11-30 13:53:13'),
(24, 0, NULL, '2014-12-08 09:09:07');

-- --------------------------------------------------------

--
-- Table structure for table `penyedias`
--

CREATE TABLE IF NOT EXISTS `penyedias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `penyedias`
--

INSERT INTO `penyedias` (`id`, `nama`, `alamat`, `telepon`) VALUES
(1, 'Daniel roy Saputrozz', 'jl-simanjutak-5c-yogyakarta', '089322123'),
(3, 'Danny Agus', 'jl jokteng 10', '09932134'),
(4, 'xx', 'xxx', '123'),
(24, 'buki', 'buki', '99999'),
(25, 'kiki', 'kiki', '9991'),
(26, 'samuel', 'handoko', '99991');

-- --------------------------------------------------------

--
-- Table structure for table `stock_shops`
--

CREATE TABLE IF NOT EXISTS `stock_shops` (
  `toko_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quatity` int(11) NOT NULL,
  `satuan_grosir` int(11) NOT NULL,
  `lusin_grosir` int(11) NOT NULL,
  `lusin6_grosir` int(11) NOT NULL,
  `satuan_eceran` int(11) NOT NULL,
  `pcs3_eceran` int(11) NOT NULL,
  `lusin1_eceran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_distribusi`
--

CREATE TABLE IF NOT EXISTS `tbl_distribusi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_kirim`
--

CREATE TABLE IF NOT EXISTS `tbl_item_kirim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distribusi_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quatity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_jual`
--

CREATE TABLE IF NOT EXISTS `tbl_nota_jual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pembeli_id` int(11) NOT NULL,
  `transjual_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `harga_total` int(11) NOT NULL,
  `dibayar` int(11) NOT NULL,
  `keuntungan_total` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pembeli`
--

CREATE TABLE IF NOT EXISTS `tbl_pembeli` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kontak` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_toko`
--

CREATE TABLE IF NOT EXISTS `tbl_toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kontak` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_jual`
--

CREATE TABLE IF NOT EXISTS `tbl_trans_jual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notajual_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `keuntungan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transbelis`
--

CREATE TABLE IF NOT EXISTS `transbelis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notabeli_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `transbelis`
--

INSERT INTO `transbelis` (`id`, `notabeli_id`, `item_id`, `kategori_id`, `quantity`, `harga`, `total`) VALUES
(1, 16, 2, 24, 10, 10000, 100000),
(2, 17, 1, 22, 3, 3000, 9000),
(12, 17, 2, 40, 444, 44, 9999),
(13, 17, 3, 43, 9000, 500, 45000),
(14, 17, 2, 40, 555, 555, 4000),
(18, 20, 2, 40, 1000, 9, 3000),
(19, 20, 2, 40, 10, 900, 0),
(20, 20, 3, 43, 10, 100, 1000),
(21, 21, 2, 40, 3, 9000, 27000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `nama_lengkap` varchar(200) DEFAULT NULL,
  `idhash` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `created`, `modified`, `nama_lengkap`, `idhash`) VALUES
(3, 'danny', '$2a$10$o8alX7lE4LzrffYHMB74A.gL7TAiyxjsrXV.tWX7vEKdLGlRVL0Ji', 'owner', '2014-12-23 03:19:01', '2014-12-23 04:12:14', 'danny aguswahyudi', '1db25bf6a7d1f0881bc7c9e7cb987d97'),
(4, 'managertoko', '$2a$10$RrFSZsaGrbAgFJn2v5poTej2dOyCpF8n7vLFHOCYlpGFaa5o6rJh.', 'manager toko', '2015-01-08 08:20:08', '2015-01-08 08:20:08', 'manager toko', '728cbe651215bf57f6d48fc44c96a6f2'),
(5, 'managergudang', '$2a$10$o/suneNNMqXQwrT3v5FSc.hijuNIwb3Jq5Rr6ClESXcvoFNfvVeMO', 'manager gudang', '2015-01-08 08:22:05', '2015-01-08 08:22:05', 'Manager Gudang', 'f25161c6eccf5621821014f8a456e627'),
(6, 'owner', '$2a$10$srLOjVFQnSFjGHO3NKYEAewvZ56BN0cBC4A.DtwoRfFWJeHzhCPyi', 'owner', '2015-01-08 08:22:28', '2015-01-08 08:22:28', 'Owner', '54240022b821569278177ef68a5639dd'),
(7, 'kasir', '$2a$10$uuv6..9Zh4zT2DX714kE7OrFggt0QVz.sjVg5xQ/NIMryYZ70Ymgq', 'kasir', '2015-01-08 08:22:54', '2015-01-08 08:22:54', 'Kasir', '15b2ae9815541d85f60b96c91e6c708d');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
